terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  project = var.gcp_project
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_network" "vpc_network" {
  name = "gitlab-pipeline"
}

module "vpc" {
  source  = "app.terraform.io/francoisneron-test/vpc/gcp"
  version = "3.0.0"
  # insert required variables here
  project_id   = var.gcp_project
  network_name = "gitlab-pipeline-module"
  shared_vpc_host = false
}